﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ICSharpCode.TextEditor.Document;
using Excel2Xml.Xml;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
//using Excel2Xml.MSInterop;
using Excel2Xml.XLSLib;
//using Excel2Xml.NOffice;

namespace Excel2Xml
{
    public partial class Xls2XmlControl : UserControl
    {
        public enum Mode
        {
            None,
            OPC,
            Language,
            Err
        }

        Mode currentMode = Mode.None;
        public Mode CurrentMode { get { return currentMode; } }

        public Xls2XmlControl()
        {
            InitializeComponent();
            textEditor.SetHighlighting("XML");
            textEditor.IndentStyle = ICSharpCode.TextEditor.Document.IndentStyle.Auto;
            textEditor.Document.FoldingManager.FoldingStrategy = new XmlFoldingStrategy();

            comboMode.Items.Add(Mode.OPC);
            comboMode.Items.Add(Mode.Language);
            comboMode.SelectedIndex = 0;

            currentMode = Mode.None;
        }

        private async void buttonLoadXls_Click(object sender, EventArgs e)
        {
            await loadAsync();
        }

        private async void buttonXml2Xls_Click(object sender, EventArgs e)
        {
            if (currentMode == Mode.Err || currentMode == Mode.None)
                return;

            var fnameOpt = currentMode == Mode.Language ? "Language.xls" : "OPCTags.xls";
            var sfd = new SaveFileDialog()
            {
                Filter = "Excel File (*.xls)|*.xls",
                FileName = fnameOpt
            };

            if (sfd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            switch (currentMode)
            {
                case Mode.None:
                    break;
                case Mode.OPC:
                case Mode.Language:
                    await exportAsync(currentMode, sfd.FileName);
                    break;
                case Mode.Err:
                    break;
                default:
                    break;
            }

        }

        private Task exportAsync(Mode mode, string filePath)
        {
            return Task.Run(() => doExport(mode, filePath));
        }

        private void doExport(Mode mode, string filePath)
        {
            var xml = this.textEditor.Document.TextContent;
            try
            {
                if (mode == Mode.Language)
                {
                    ExportHelper.Language(xml, filePath);
                }
                else if (mode == Mode.OPC)
                {
                    ExportHelper.Opc(xml, filePath);
                }

                Invoke(new Action(() => MessageBox.Show(this, "File esportato correttamente")));
            }
            catch (Exception exc)
            {
                Invoke(new Action(() => MessageBox.Show(this, string.Format("Errore: {0}", exc.Message))));
            }
        }



        void SetWait(bool wait)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() => SetWait(wait)));
                return;
            }

            if (wait)
                pictureWait.BringToFront();
            else
                pictureWait.SendToBack();

            pictureWait.Enabled = pictureWait.Visible = wait;
        }

        public event Action<string> FileLoaded = (f) => { };

        Mode filename2Mode(string fname)
        {
            if (fname == "Language.xls")
                return Mode.Language;
            else if (fname == "OPCTags.xls")
                return Mode.OPC;
            else
                return Mode.Err;
        }

        private async Task loadAsync()
        {
            var ofd = new OpenFileDialog()
            {
                Filter = "Excel File (*.xls)|*.xls",
            };

            if (ofd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            //var mode = filename2Mode(Path.GetFileName(ofd.FileName));
            var mode = (Mode)comboMode.SelectedItem;
            if (mode != Mode.Err)
            {


                await Task.Run(() =>
                {
                    try
                    {
                        SetWait(true);
                        var text = xls2xml(mode, ofd.FileName);


                        textEditor.Document.TextContent = text;

                    }
                    catch (Exception exc)
                    {
                        //MessageBox.Show(this, exc.Message);
                        SetWait(false);
                    }

                    Invoke(new Action(() =>
                    {
                        textEditor.Refresh();
                        textEditor.Document.FoldingManager.UpdateFoldings(null, null);
                        FileLoaded(ofd.FileName);
                    }));

                    currentMode = mode;
                    SetWait(false);
                });
            }
            else
            {
                MessageBox.Show(this, string.Format("File non supportato", ofd.FileName));
            }
        }

        private string xls2xml(Mode mode, string file)
        {
            switch (mode)
            {
                case Mode.OPC:
                    return xls2xmlOpc(file);
                case Mode.Language:
                    return xls2xmlLanguage(file);
                case Mode.Err:
                default:
                    return "";
            }

            return "";
        }

        private string xls2xmlOpc(string file)
        {
            if (InvokeRequired)
            {
                return (string)Invoke(new Func<string>(() => xls2xmlOpc(file)));
            }

            return LoadHelper.Opc(file);

        }

        private string xls2xmlLanguage(string file)
        {
            if (InvokeRequired)
            {
                return (string)Invoke(new Func<string>(() => xls2xmlLanguage(file)));
            }

            return LoadHelper.Language(file);
        }

    }
}
