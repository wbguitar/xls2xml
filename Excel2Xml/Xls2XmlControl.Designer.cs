﻿namespace Excel2Xml
{
    partial class Xls2XmlControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textEditor = new ICSharpCode.TextEditor.TextEditorControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonLoadXls = new System.Windows.Forms.Button();
            this.buttonXml2Xls = new System.Windows.Forms.Button();
            this.pictureWait = new System.Windows.Forms.PictureBox();
            this.comboMode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureWait)).BeginInit();
            this.SuspendLayout();
            // 
            // textEditor
            // 
            this.textEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEditor.Font = new System.Drawing.Font("Courier New", 10F);
            this.textEditor.IsReadOnly = false;
            this.textEditor.LineViewerStyle = ICSharpCode.TextEditor.Document.LineViewerStyle.FullRow;
            this.textEditor.Location = new System.Drawing.Point(0, 31);
            this.textEditor.Name = "textEditor";
            this.textEditor.ShowMatchingBracket = false;
            this.textEditor.ShowSpaces = true;
            this.textEditor.ShowTabs = true;
            this.textEditor.Size = new System.Drawing.Size(558, 354);
            this.textEditor.TabIndex = 4;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.comboMode);
            this.flowLayoutPanel1.Controls.Add(this.buttonLoadXls);
            this.flowLayoutPanel1.Controls.Add(this.buttonXml2Xls);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(558, 31);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // buttonLoadXls
            // 
            this.buttonLoadXls.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoadXls.Location = new System.Drawing.Point(205, 3);
            this.buttonLoadXls.Name = "buttonLoadXls";
            this.buttonLoadXls.Size = new System.Drawing.Size(105, 23);
            this.buttonLoadXls.TabIndex = 0;
            this.buttonLoadXls.Text = "Load XLS";
            this.buttonLoadXls.UseVisualStyleBackColor = true;
            this.buttonLoadXls.Click += new System.EventHandler(this.buttonLoadXls_Click);
            // 
            // buttonXml2Xls
            // 
            this.buttonXml2Xls.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonXml2Xls.Location = new System.Drawing.Point(316, 3);
            this.buttonXml2Xls.Name = "buttonXml2Xls";
            this.buttonXml2Xls.Size = new System.Drawing.Size(105, 23);
            this.buttonXml2Xls.TabIndex = 1;
            this.buttonXml2Xls.Text = "Export XLS";
            this.buttonXml2Xls.UseVisualStyleBackColor = true;
            this.buttonXml2Xls.Click += new System.EventHandler(this.buttonXml2Xls_Click);
            // 
            // pictureWait
            // 
            this.pictureWait.BackColor = System.Drawing.Color.White;
            this.pictureWait.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureWait.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureWait.Image = global::Excel2Xml.Properties.Resources.wait;
            this.pictureWait.Location = new System.Drawing.Point(0, 31);
            this.pictureWait.Name = "pictureWait";
            this.pictureWait.Size = new System.Drawing.Size(558, 354);
            this.pictureWait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureWait.TabIndex = 6;
            this.pictureWait.TabStop = false;
            this.pictureWait.Visible = false;
            // 
            // comboMode
            // 
            this.comboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMode.FormattingEnabled = true;
            this.comboMode.Location = new System.Drawing.Point(78, 3);
            this.comboMode.Name = "comboMode";
            this.comboMode.Size = new System.Drawing.Size(121, 21);
            this.comboMode.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 26);
            this.label1.TabIndex = 3;
            this.label1.Text = "Load Mode";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Xls2XmlControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureWait);
            this.Controls.Add(this.textEditor);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "Xls2XmlControl";
            this.Size = new System.Drawing.Size(558, 385);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureWait)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.TextEditor.TextEditorControl textEditor;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttonLoadXls;
        private System.Windows.Forms.PictureBox pictureWait;
        private System.Windows.Forms.Button buttonXml2Xls;
        private System.Windows.Forms.ComboBox comboMode;
        private System.Windows.Forms.Label label1;
    }
}
