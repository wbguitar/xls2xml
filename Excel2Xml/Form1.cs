﻿using Excel2Xml.Xml;
using ExcelLibrary.SpreadSheet;
using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Excel2Xml
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            tabControl1.TabPages.Clear();
            AddTab();
        }

        private void buttonAddTab_Click(object sender, EventArgs e)
        {
            AddTab();
        }

        void AddTab()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(AddTab));
                return;
            }

            var page = new TabPage()
            {
                Text = string.Format("Tab page {0}", tabControl1.TabPages.Count + 1)
            };

            var control = new Xls2XmlControl() { Dock = DockStyle.Fill };
            page.Controls.Add(control);
            control.FileLoaded += (f) => 
                {
                    var fname = Path.GetFileNameWithoutExtension(f);
                    var count = tabControl1.TabPages
                        .Cast<TabPage>()
                        .Select(p => p.Name.Contains(fname) ? 1 : 0)
                        .Sum();

                    page.Text = string.Format("{0} {1}", fname, count + 1);
                };
            this.tabControl1.TabPages.Add(page);
        }

        private void buttonCloseTab_Click(object sender, EventArgs e)
        {
            var tab = this.tabControl1.SelectedTab;
            if (tab != null)
            {
                var found = tab.Controls.Cast<Control>().FirstOrDefault(c => c.GetType() == typeof(Xls2XmlControl)) as Xls2XmlControl;
                if (found == null)
                    return;

                if ((found.CurrentMode != Xls2XmlControl.Mode.None && found.CurrentMode != Xls2XmlControl.Mode.Err) &&
                    MessageBox.Show(this, "La tab corrente verrà chiusa, continuare?", "", MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
                    return;

                tabControl1.TabPages.Remove(tab);
            }
        }
    }
}
