﻿using ExcelLibrary.SpreadSheet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Excel2Xml.XLSLib
{
    public static class ExportHelper
    {
        static T xlsFromXml<T>(string xml)
        {
            T xls = default(T);
            using (var rd = XmlReader.Create(new StringReader(xml)))
            {
                var ser = new XmlSerializer(typeof(T));
                xls = (T)ser.Deserialize(rd);
            }

            return xls;
        }

        public static void Language(string xml, string filePath)
        {
            var xls = xlsFromXml<LanguageXls>(xml);

            var wb = new Workbook();

            var wsLang = new Worksheet("Translations");
            var wsMaint = new Worksheet("Maintenance");
            var wsGroup = new Worksheet("Group");
            var wsFreq = new Worksheet("Frequency");

            // headers
            setLangHeaders(wsLang, "ID");
            setLangHeaders(wsMaint, "CODE");
            setLangHeaders(wsGroup, "CODE");
            setLangHeaders(wsFreq, "CODE");

            setLangWorksheet(wsLang, xls.Translations);
            setLangWorksheet(wsMaint, xls.Maintenance);
            setLangWorksheet(wsGroup, xls.Group);
            setLangWorksheet(wsFreq, xls.Frequency);

            wb.Worksheets.AddRange(new Worksheet[]
                {
                    wsLang,
                    wsMaint,
                    wsGroup,
                    wsFreq,
                });

            wb.Save(filePath);
        }

        static CellStyle greyStyle = new CellStyle() { BackColor = Color.Gray };

        static void setLangWorksheet(Worksheet ws, IList<LangRows> rows)
        {
            for (int i = 1; i < rows.Count; i++)
            {
                var row = rows[i - 1];
                ws.Cells[i, 0] = new Cell(row.ID);
                ws.Cells[i, 1] = new Cell(row.ITALIANO.CellValue);
                ws.Cells[i, 2] = new Cell(row.INGLESE.CellValue);
                ws.Cells[i, 3] = new Cell(row.FRANCESE.CellValue);
                ws.Cells[i, 4] = new Cell(row.TEDESCO.CellValue);
                ws.Cells[i, 5] = new Cell(row.SPAGNOLO.CellValue);
                ws.Cells[i, 6] = new Cell(row.RUSSO.CellValue);
                ws.Cells[i, 7] = new Cell(row.CINESE.CellValue);
                ws.Cells[i, 8] = new Cell(row.RUMENO.CellValue);
                ws.Cells[i, 9] = new Cell(row.PORTOGHESE.CellValue);
                ws.Cells[i, 10] = new Cell(row.POLACCO.CellValue);
                ws.Cells[i, 11] = new Cell(row.BULGARO.CellValue);
                ws.Cells[i, 12] = new Cell(row.TURCO.CellValue);
                ws.Cells[i, 13] = new Cell(row.GRECO.CellValue);

                ws.Cells[i, 0].Style = greyStyle;
                ws.Cells[i, 1].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.ITALIANO.BackColor) };
                ws.Cells[i, 2].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.INGLESE.BackColor) };
                ws.Cells[i, 3].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.FRANCESE.BackColor) };
                ws.Cells[i, 4].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.TEDESCO.BackColor) };
                ws.Cells[i, 5].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.SPAGNOLO.BackColor) };
                ws.Cells[i, 6].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.RUSSO.BackColor) };
                ws.Cells[i, 7].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.CINESE.BackColor) };
                ws.Cells[i, 8].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.RUMENO.BackColor) };
                ws.Cells[i, 9].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.PORTOGHESE.BackColor) };
                ws.Cells[i, 10].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.POLACCO.BackColor) };
                ws.Cells[i, 11].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.BULGARO.BackColor) };
                ws.Cells[i, 12].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.TURCO.BackColor) };
                ws.Cells[i, 13].Style = new CellStyle() { BackColor = ColorTranslator.FromHtml(row.GRECO.BackColor) };
            }
        }

        static void setLangHeaders(Worksheet ws, string idHead)
        {
            //var row = ws.Cells.Rows[0];

            ws.Cells[0, 0] = new Cell(idHead) { Style = greyStyle };
            ws.Cells[0, 1] = new Cell("ITALIANO") { Style = greyStyle };
            ws.Cells[0, 2] = new Cell("INGLESE") { Style = greyStyle };
            ws.Cells[0, 3] = new Cell("FRANCESE") { Style = greyStyle };
            ws.Cells[0, 4] = new Cell("TEDESCO") { Style = greyStyle };
            ws.Cells[0, 5] = new Cell("SPAGNOLO") { Style = greyStyle };
            ws.Cells[0, 6] = new Cell("RUSSO") { Style = greyStyle };
            ws.Cells[0, 7] = new Cell("CINESE") { Style = greyStyle };
            ws.Cells[0, 8] = new Cell("RUMENO") { Style = greyStyle };
            ws.Cells[0, 9] = new Cell("PORTOGHESE") { Style = greyStyle };
            ws.Cells[0, 10] = new Cell("POLACCO") { Style = greyStyle };
            ws.Cells[0, 11] = new Cell("BULGARO") { Style = greyStyle };
            ws.Cells[0, 12] = new Cell("TURCO") { Style = greyStyle };
            ws.Cells[0, 13] = new Cell("GRECO") { Style = greyStyle };
        }

        public static void Opc(string xml, string filePath)
        {
            var xls = xlsFromXml<OpcTagsXls>(xml);

            var wb = new Workbook();

            var plc4 = new Worksheet("plc4");
            var plc2 = new Worksheet("plc2");
            var plc2_Reworked = new Worksheet("plc2_Reworked");
            var WamFoam = new Worksheet("WamFoam");
            var plc4cist = new Worksheet("plc4cist");
            var plc5 = new Worksheet("plc5");

            setOpcWorksheet(plc4, xls.plc4);
            setOpcWorksheet(plc2, xls.plc2);
            setOpcWorksheet(plc2_Reworked, xls.plc2_Reworked);
            setOpcWorksheet(WamFoam, xls.WamFoam);
            setOpcWorksheet(plc4cist, xls.plc4cist);
            setOpcWorksheet(plc5, xls.plc5);

            wb.Worksheets.AddRange(new Worksheet[]
                {
                    plc4,
                    plc2,
                    plc2_Reworked,
                    WamFoam,
                    plc4cist,
                    plc5
                });

            wb.Save(filePath);
        }

        static void setOpcWorksheet(Worksheet ws, IList<TagRow> rows)
        {
            for (int i = 1; i < rows.Count; i++)
            {
                var row = rows[i - 1];
                ws.Cells[i, 0] = new Cell(row.Tag);
                ws.Cells[i, 1] = new Cell(row.Address);
                for (int j = 2; j < row.Note.Count; j++)
                {
                    ws.Cells[i, j] = new Cell(row.Note[j]);
                }
            }
        }

    }

    public static class LoadHelper
    {
        static Workbook LoadWb(string fname)
        {
            return Workbook.Load(File.Open(fname, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)); // in questo modo apre il file anche se gia` aperto su excel
        }

        public static string Language(string fname)
        {
            var wb = LoadWb(fname);

            var translations = wb.Worksheets[0].Cells;
            var maintenance = wb.Worksheets[1].Cells;
            var groups = wb.Worksheets[2].Cells;
            var frequency = wb.Worksheets[3].Cells;

            var xls = new LanguageXls();

            var createRow = new Func<Row, LangRows>((row) =>
            {
                var createCell = new Func<int, LangCell>((i) =>
                {
                    var cell = row.GetCell(i);
                    var cellValue = cell.StringValue;
                    var style = cell.Style;
                    return new LangCell() { CellValue = cellValue, BackColor = style == null ? "" : ColorTranslator.ToHtml(style.BackColor) };
                });

                return new LangRows()
                {
                    ID = row.GetCell(0).StringValue,
                    ITALIANO = createCell(1),
                    INGLESE = createCell(2),
                    FRANCESE = createCell(3),
                    TEDESCO = createCell(4),
                    SPAGNOLO = createCell(5),
                    RUSSO = createCell(6),
                    CINESE = createCell(7),
                    RUMENO = createCell(8),
                    PORTOGHESE = createCell(9),
                    POLACCO = createCell(10),
                    BULGARO = createCell(11),
                    TURCO = createCell(12),
                    GRECO = createCell(13),
                };
            });

            for (int i = 1; i < translations.Rows.Count; i++)
            {
                var row = translations.Rows[i];
                xls.Translations.Add(createRow(row));
            }

            for (int i = 1; i < maintenance.Rows.Count; i++)
            {
                var row = maintenance.Rows[i];
                xls.Maintenance.Add(createRow(row));
            }

            for (int i = 1; i < groups.Rows.Count; i++)
            {
                var row = groups.Rows[i];
                xls.Group.Add(createRow(row));
            }

            for (int i = 1; i < frequency.Rows.Count; i++)
            {
                var row = frequency.Rows[i];
                xls.Frequency.Add(createRow(row));
            }

            return objToXml(xls);
        }

        private static string objToXml(object xls)
        {
            var sb = new StringBuilder();
            var settings = new XmlWriterSettings()
            {
                OmitXmlDeclaration = true,
                Indent = true,
            };

            using (var wr = XmlTextWriter.Create(sb, settings))
            {
                var ser = new XmlSerializer(xls.GetType());
                ser.Serialize(wr, xls);
            }

            return sb.ToString();
        }

        public static string Opc(string file)
        {
            Workbook wb = LoadWb(file);

            var plc4 = wb.Worksheets.Count > 0 ? wb.Worksheets[0].Cells : null;
            var plc2 = wb.Worksheets.Count > 1 ? wb.Worksheets[1].Cells : null;
            var plc2_Reworked = wb.Worksheets.Count > 2 ? wb.Worksheets[2].Cells : null;
            var WamFoam = wb.Worksheets.Count > 3 ? wb.Worksheets[3].Cells : null;
            var plc4cist = wb.Worksheets.Count > 4 ? wb.Worksheets[4].Cells : null;
            var plc5 = wb.Worksheets.Count > 5 ? wb.Worksheets[5].Cells : null;

            if (plc4 == null) // nessun worksheet
                return "";

            var xls = new OpcTagsXls();

            var createRow = new Func<Row, TagRow>((row) =>
            {
                var arr = new List<string>();
                for (int i = 2; i < row.LastColIndex + 1; i++)
                {
                    arr.Add(row.GetCell(i).StringValue.Trim());
                }
                return new TagRow()
                {
                    Tag = row.GetCell(0).StringValue.Trim(),
                    Address = row.GetCell(1).StringValue.Trim(),
                    Note = arr
                };
            });

            for (int i = 1; i < plc4.Rows.Count; i++)
            {
                var row = plc4.Rows[i];
                xls.plc4.Add(createRow(row));
            }
            if (plc2 != null)
            {
                for (int i = 1; i < plc2.Rows.Count; i++)
                {
                    var row = plc2.Rows[i];
                    xls.plc2.Add(createRow(row));
                } 
            }
            if (plc2_Reworked != null)
            {
                for (int i = 1; i < plc2_Reworked.Rows.Count; i++)
                {
                    var row = plc2_Reworked.Rows[i];
                    xls.plc2_Reworked.Add(createRow(row));
                } 
            }
            if (WamFoam != null)
            {
                for (int i = 1; i < WamFoam.Rows.Count; i++)
                {
                    var row = WamFoam.Rows[i];
                    xls.WamFoam.Add(createRow(row));
                } 
            }
            if (plc4cist != null)
            {
                for (int i = 1; i < plc4cist.Rows.Count; i++)
                {
                    var row = plc4cist.Rows[i];
                    xls.plc4cist.Add(createRow(row));
                } 
            }
            if (plc5 != null)
            {
                for (int i = 1; i < plc5.Rows.Count; i++)
                {
                    var row = plc5.Rows[i];
                    xls.plc5.Add(createRow(row));
                } 
            }

            return objToXml(xls);
        }
    }


    //namespace Netoffice
    //{
    //    using Microsoft.Office.Interop.Excel;
    //    using XLS = Microsoft.Office.Interop.Excel;


    //    public static class ExportHelper
    //    {
    //        public static void Language(string xml, string filePath)
    //        {
    //            Application app = new Application();

    //            var xls = new LanguageXls();
    //            using (var rd = XmlReader.Create(new StringReader(xml)))
    //            {
    //                var ser = new XmlSerializer(typeof(LanguageXls));
    //                xls = ser.Deserialize(rd) as LanguageXls;
    //            }

    //            var wb = app.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);

    //            var wsLang = wb.Worksheets[1] as Worksheet;// wb.Worksheets.Add("Translations") as Worksheet;
    //            var wsMaint = app.Worksheets.Add() as Worksheet;//wb.Worksheets.Add("Maintenance") as Worksheet;
    //            var wsGroup = app.Worksheets.Add() as Worksheet;// wb.Worksheets.Add("Group") as Worksheet;
    //            var wsFreq = app.Worksheets.Add() as Worksheet;//wb.Worksheets.Add("Frequency") as Worksheet;

    //            wsLang.Name = "Translations";
    //            wsMaint.Name = "Maintenance";
    //            wsGroup.Name = "Group";
    //            wsFreq.Name = "Frequency";

    //            // headers
    //            setHeaders(wsLang, "ID");
    //            setHeaders(wsMaint, "CODE");
    //            setHeaders(wsGroup, "CODE");
    //            setHeaders(wsFreq, "CODE");

    //            setWorksheet(wsLang, xls.Translations);
    //            setWorksheet(wsLang, xls.Maintenance);
    //            setWorksheet(wsLang, xls.Group);
    //            setWorksheet(wsLang, xls.Translations);

    //            //wb.Worksheets.Add(new Worksheet[]
    //            //{
    //            //    wsLang,
    //            //    wsMaint,
    //            //    wsGroup,
    //            //    wsFreq,
    //            //});

    //            wb.SaveAs(filePath);
    //        }


    //        static void setWorksheet(Worksheet ws, IList<LangRows> rows)
    //        {
    //            //Style greyStyle = new Style();

    //            for (int i = 1; i < rows.Count; i++)
    //            {
    //                //ws.Range[string.Format("A{0}", i), string.Format("N{0}", i)].Insert();

    //                var row = rows[i - 1];
    //                var range = ws.get_Range(string.Format("A{0}", i + 1), string.Format("N{0}", i)) as Range;

    //                range[1].Value = row.ID;
    //                range[2].Value = row.ITALIANO.CellValue;
    //                range[3].Value = row.INGLESE.CellValue;
    //                range[4].Value = row.FRANCESE.CellValue;
    //                range[5].Value = row.TEDESCO.CellValue;
    //                range[6].Value = row.SPAGNOLO.CellValue;
    //                range[7].Value = row.RUSSO.CellValue;
    //                range[8].Value = row.CINESE.CellValue;
    //                range[9].Value = row.RUMENO.CellValue;
    //                range[10].Value = row.PORTOGHESE.CellValue;
    //                range[11].Value = row.POLACCO.CellValue;
    //                range[12].Value = row.BULGARO.CellValue;
    //                range[13].Value = row.TURCO.CellValue;
    //                range[14].Value = row.GRECO.CellValue;

    //                range[1].Interior.Color = Color.Gray;
    //                range[2].Interior.Color = ColorTranslator.FromHtml(row.ITALIANO.BackColor);
    //                range[3].Interior.Color = ColorTranslator.FromHtml(row.INGLESE.BackColor);
    //                range[4].Interior.Color = ColorTranslator.FromHtml(row.FRANCESE.BackColor);
    //                range[5].Interior.Color = ColorTranslator.FromHtml(row.TEDESCO.BackColor);
    //                range[6].Interior.Color = ColorTranslator.FromHtml(row.SPAGNOLO.BackColor);
    //                range[7].Interior.Color = ColorTranslator.FromHtml(row.RUSSO.BackColor);
    //                range[8].Interior.Color = ColorTranslator.FromHtml(row.CINESE.BackColor);
    //                range[9].Interior.Color = ColorTranslator.FromHtml(row.RUMENO.BackColor);
    //                range[10].Interior.Color = ColorTranslator.FromHtml(row.PORTOGHESE.BackColor);
    //                range[11].Interior.Color = ColorTranslator.FromHtml(row.POLACCO.BackColor);
    //                range[12].Interior.Color = ColorTranslator.FromHtml(row.BULGARO.BackColor);
    //                range[13].Interior.Color = ColorTranslator.FromHtml(row.TURCO.BackColor);
    //                range[14].Interior.Color = ColorTranslator.FromHtml(row.GRECO.BackColor);

    //                //ws.Cells[i + 1, 1].Value = row.ID;
    //                //ws.Cells[i + 1, 2].Value = row.ITALIANO.CellValue;
    //                //ws.Cells[i + 1, 3].Value = row.INGLESE.CellValue;
    //                //ws.Cells[i + 1, 4].Value = row.FRANCESE.CellValue;
    //                //ws.Cells[i + 1, 5].Value = row.TEDESCO.CellValue;
    //                //ws.Cells[i + 1, 6].Value = row.SPAGNOLO.CellValue;
    //                //ws.Cells[i + 1, 7].Value = row.RUSSO.CellValue;
    //                //ws.Cells[i + 1, 8].Value = row.CINESE.CellValue;
    //                //ws.Cells[i + 1, 9].Value = row.RUMENO.CellValue;
    //                //ws.Cells[i + 1, 10].Value = row.PORTOGHESE.CellValue;
    //                //ws.Cells[i + 1, 11].Value = row.POLACCO.CellValue;
    //                //ws.Cells[i + 1, 12].Value = row.BULGARO.CellValue;
    //                //ws.Cells[i + 1, 13].Value = row.TURCO.CellValue;
    //                //ws.Cells[i + 1, 14].Value = row.GRECO.CellValue;

    //                //ws.Cells[i + 1, 1].Interior.Color = Color.Gray;
    //                //ws.Cells[i + 1, 2].Interior.Color = ColorTranslator.FromHtml(row.ITALIANO.BackColor);
    //                //ws.Cells[i + 1, 3].Interior.Color = ColorTranslator.FromHtml(row.INGLESE.BackColor);
    //                //ws.Cells[i + 1, 4].Interior.Color = ColorTranslator.FromHtml(row.FRANCESE.BackColor);
    //                //ws.Cells[i + 1, 5].Interior.Color = ColorTranslator.FromHtml(row.TEDESCO.BackColor);
    //                //ws.Cells[i + 1, 6].Interior.Color = ColorTranslator.FromHtml(row.SPAGNOLO.BackColor);
    //                //ws.Cells[i + 1, 7].Interior.Color = ColorTranslator.FromHtml(row.RUSSO.BackColor);
    //                //ws.Cells[i + 1, 8].Interior.Color = ColorTranslator.FromHtml(row.CINESE.BackColor);
    //                //ws.Cells[i + 1, 9].Interior.Color = ColorTranslator.FromHtml(row.RUMENO.BackColor);
    //                //ws.Cells[i + 1, 10].Interior.Color = ColorTranslator.FromHtml(row.PORTOGHESE.BackColor);
    //                //ws.Cells[i + 1, 11].Interior.Color = ColorTranslator.FromHtml(row.POLACCO.BackColor);
    //                //ws.Cells[i + 1, 12].Interior.Color = ColorTranslator.FromHtml(row.BULGARO.BackColor);
    //                //ws.Cells[i + 1, 13].Interior.Color = ColorTranslator.FromHtml(row.TURCO.BackColor);
    //                //ws.Cells[i + 1, 14].Interior.Color = ColorTranslator.FromHtml(row.GRECO.BackColor);
    //            }
    //        }

    //        static void setHeaders(Worksheet ws, string idHead)
    //        {
    //            //ws.Range["A0", "N0"].Insert();

    //            //var row = ws.Cells.Rows[0];
    //            (ws.Cells[1, 1] as Range).Value = idHead;
    //            (ws.Cells[1, 2] as Range).Value = "ITALIANO";
    //            (ws.Cells[1, 3] as Range).Value = "INGLESE";
    //            (ws.Cells[1, 4] as Range).Value = "FRANCESE";
    //            (ws.Cells[1, 5] as Range).Value = "TEDESCO";
    //            (ws.Cells[1, 6] as Range).Value = "SPAGNOLO";
    //            (ws.Cells[1, 7] as Range).Value = "RUSSO";
    //            (ws.Cells[1, 8] as Range).Value = "CINESE";
    //            (ws.Cells[1, 9] as Range).Value = "RUMENO";
    //            (ws.Cells[1, 10] as Range).Value = "PORTOGHESE";
    //            (ws.Cells[1, 11] as Range).Value = "POLACCO";
    //            (ws.Cells[1, 12] as Range).Value = "BULGARO";
    //            (ws.Cells[1, 13] as Range).Value = "TURCO";
    //            (ws.Cells[1, 14] as Range).Value = "GRECO";

    //            (ws.Cells[1, 1] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 2] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 3] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 4] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 5] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 6] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 7] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 8] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 9] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 10] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 11] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 12] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 13] as Range).Interior.Color = Color.Gray;
    //            (ws.Cells[1, 14] as Range).Interior.Color = Color.Gray;
    //        }

    //        public static void Opc(string xml, string filePath)
    //        {
    //            throw new NotImplementedException();
    //        }
    //    }

    //    public static class LoadHelper
    //    {
    //        public static string Language(Workbook wb)
    //        {
    //            var translations = wb.Worksheets[0];
    //            var maintenance = wb.Worksheets[1];
    //            var groups = wb.Worksheets[2];
    //            var frequency = wb.Worksheets[3];

    //            var xls = new LanguageXls();

    //            var createRow = new Func<Range, LangRows>((row) =>
    //            {
    //                var createCell = new Func<int, LangCell>((i) =>
    //                {
    //                    var cell = row[0, i];
    //                    var cellValue = cell.Value.ToString();
    //                    return new LangCell() { CellValue = cellValue, BackColor = ColorTranslator.ToHtml((Color)cell.Interior.Color) };
    //                });

    //                return new LangRows()
    //                {
    //                    ID = row[0, 0].Value.ToString(),
    //                    ITALIANO = createCell(1),
    //                    INGLESE = createCell(2),
    //                    FRANCESE = createCell(3),
    //                    TEDESCO = createCell(4),
    //                    SPAGNOLO = createCell(5),
    //                    RUSSO = createCell(6),
    //                    CINESE = createCell(7),
    //                    RUMENO = createCell(8),
    //                    PORTOGHESE = createCell(9),
    //                    POLACCO = createCell(10),
    //                    BULGARO = createCell(11),
    //                    TURCO = createCell(12),
    //                    GRECO = createCell(13),
    //                };
    //            });

    //            //for (int i = 1; i < translations.Rows.Count; i++)
    //            //{
    //            //    var row = translations.Rows[i];
    //            //    xls.Translations.Add(createRow(row));
    //            //}

    //            //for (int i = 1; i < maintenance.Rows.Count; i++)
    //            //{
    //            //    var row = maintenance.Rows[i];
    //            //    xls.Maintenance.Add(createRow(row));
    //            //}

    //            //for (int i = 1; i < groups.Rows.Count; i++)
    //            //{
    //            //    var row = groups.Rows[i];
    //            //    xls.Group.Add(createRow(row));
    //            //}

    //            //for (int i = 1; i < frequency.Rows.Count; i++)
    //            //{
    //            //    var row = frequency.Rows[i];
    //            //    xls.Frequency.Add(createRow(row));
    //            //}

    //            var sb = new StringBuilder();
    //            var settings = new XmlWriterSettings()
    //            {
    //                OmitXmlDeclaration = true,
    //                Indent = true,

    //            };

    //            using (var wr = XmlTextWriter.Create(sb, settings))
    //            {
    //                var ser = new XmlSerializer(typeof(LanguageXls));
    //                ser.Serialize(wr, xls);
    //            }

    //            return sb.ToString();
    //        }

    //        public static string Opc(Workbook wb)
    //        {
    //            throw new NotImplementedException();
    //        }
    //    }
    //}
}
