﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excel2Xml.MSInterop
{
    using Microsoft.Office.Interop.Excel;
    using System.Drawing;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;
    using XLS = Microsoft.Office.Interop.Excel;

    public static class ExportHelper
    {
        static Application app = new Application();

        public static void Language(string xml, string filePath)
        {
           

            var xls = new LanguageXls();
            using (var rd = XmlReader.Create(new StringReader(xml)))
            {
                var ser = new XmlSerializer(typeof(LanguageXls));
                xls = ser.Deserialize(rd) as LanguageXls;
            }

            var wb = app.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);

            var wsLang = wb.Worksheets[1] as Worksheet; // wb.Worksheets.Add("Translations") as Worksheet;
            var wsMaint = app.Worksheets.Add() as Worksheet; //wb.Worksheets.Add("Maintenance") as Worksheet;
            var wsGroup = app.Worksheets.Add() as Worksheet; // wb.Worksheets.Add("Group") as Worksheet;
            var wsFreq = app.Worksheets.Add() as Worksheet; //wb.Worksheets.Add("Frequency") as Worksheet;

            wsLang.Name = "Translations";
            wsMaint.Name = "Maintenance";
            wsGroup.Name = "Group";
            wsFreq.Name = "Frequency";

            // headers
            setHeaders(wsLang, "ID");
            setHeaders(wsMaint, "CODE");
            setHeaders(wsGroup, "CODE");
            setHeaders(wsFreq, "CODE");

            setWorksheet(wsLang, xls.Translations);
            setWorksheet(wsLang, xls.Maintenance);
            setWorksheet(wsLang, xls.Group);
            setWorksheet(wsLang, xls.Translations);

            //wb.Worksheets.Add(new Worksheet[]
            //{
            //    wsLang,
            //    wsMaint,
            //    wsGroup,
            //    wsFreq,
            //});

            wb.SaveAs(filePath);
        }


        static void setWorksheet(Worksheet ws, IList<LangRows> rows)
        {
            //Style greyStyle = new Style();

            for (int i = 1; i < rows.Count; i++)
            {
                //ws.Range[string.Format("A{0}", i), string.Format("N{0}", i)].Insert();

                var row = rows[i - 1];
                var range = ws.get_Range(string.Format("A{0}", i + 1), string.Format("N{0}", i)) as Range;

                range[1].Value = row.ID;
                range[2].Value = row.ITALIANO.CellValue;
                range[3].Value = row.INGLESE.CellValue;
                range[4].Value = row.FRANCESE.CellValue;
                range[5].Value = row.TEDESCO.CellValue;
                range[6].Value = row.SPAGNOLO.CellValue;
                range[7].Value = row.RUSSO.CellValue;
                range[8].Value = row.CINESE.CellValue;
                range[9].Value = row.RUMENO.CellValue;
                range[10].Value = row.PORTOGHESE.CellValue;
                range[11].Value = row.POLACCO.CellValue;
                range[12].Value = row.BULGARO.CellValue;
                range[13].Value = row.TURCO.CellValue;
                range[14].Value = row.GRECO.CellValue;

                range[1].Interior.Color = Color.Gray;
                range[2].Interior.Color = ColorTranslator.FromHtml(row.ITALIANO.BackColor);
                range[3].Interior.Color = ColorTranslator.FromHtml(row.INGLESE.BackColor);
                range[4].Interior.Color = ColorTranslator.FromHtml(row.FRANCESE.BackColor);
                range[5].Interior.Color = ColorTranslator.FromHtml(row.TEDESCO.BackColor);
                range[6].Interior.Color = ColorTranslator.FromHtml(row.SPAGNOLO.BackColor);
                range[7].Interior.Color = ColorTranslator.FromHtml(row.RUSSO.BackColor);
                range[8].Interior.Color = ColorTranslator.FromHtml(row.CINESE.BackColor);
                range[9].Interior.Color = ColorTranslator.FromHtml(row.RUMENO.BackColor);
                range[10].Interior.Color = ColorTranslator.FromHtml(row.PORTOGHESE.BackColor);
                range[11].Interior.Color = ColorTranslator.FromHtml(row.POLACCO.BackColor);
                range[12].Interior.Color = ColorTranslator.FromHtml(row.BULGARO.BackColor);
                range[13].Interior.Color = ColorTranslator.FromHtml(row.TURCO.BackColor);
                range[14].Interior.Color = ColorTranslator.FromHtml(row.GRECO.BackColor);

                //ws.Cells[i + 1, 1].Value = row.ID;
                //ws.Cells[i + 1, 2].Value = row.ITALIANO.CellValue;
                //ws.Cells[i + 1, 3].Value = row.INGLESE.CellValue;
                //ws.Cells[i + 1, 4].Value = row.FRANCESE.CellValue;
                //ws.Cells[i + 1, 5].Value = row.TEDESCO.CellValue;
                //ws.Cells[i + 1, 6].Value = row.SPAGNOLO.CellValue;
                //ws.Cells[i + 1, 7].Value = row.RUSSO.CellValue;
                //ws.Cells[i + 1, 8].Value = row.CINESE.CellValue;
                //ws.Cells[i + 1, 9].Value = row.RUMENO.CellValue;
                //ws.Cells[i + 1, 10].Value = row.PORTOGHESE.CellValue;
                //ws.Cells[i + 1, 11].Value = row.POLACCO.CellValue;
                //ws.Cells[i + 1, 12].Value = row.BULGARO.CellValue;
                //ws.Cells[i + 1, 13].Value = row.TURCO.CellValue;
                //ws.Cells[i + 1, 14].Value = row.GRECO.CellValue;

                //ws.Cells[i + 1, 1].Interior.Color = Color.Gray;
                //ws.Cells[i + 1, 2].Interior.Color = ColorTranslator.FromHtml(row.ITALIANO.BackColor);
                //ws.Cells[i + 1, 3].Interior.Color = ColorTranslator.FromHtml(row.INGLESE.BackColor);
                //ws.Cells[i + 1, 4].Interior.Color = ColorTranslator.FromHtml(row.FRANCESE.BackColor);
                //ws.Cells[i + 1, 5].Interior.Color = ColorTranslator.FromHtml(row.TEDESCO.BackColor);
                //ws.Cells[i + 1, 6].Interior.Color = ColorTranslator.FromHtml(row.SPAGNOLO.BackColor);
                //ws.Cells[i + 1, 7].Interior.Color = ColorTranslator.FromHtml(row.RUSSO.BackColor);
                //ws.Cells[i + 1, 8].Interior.Color = ColorTranslator.FromHtml(row.CINESE.BackColor);
                //ws.Cells[i + 1, 9].Interior.Color = ColorTranslator.FromHtml(row.RUMENO.BackColor);
                //ws.Cells[i + 1, 10].Interior.Color = ColorTranslator.FromHtml(row.PORTOGHESE.BackColor);
                //ws.Cells[i + 1, 11].Interior.Color = ColorTranslator.FromHtml(row.POLACCO.BackColor);
                //ws.Cells[i + 1, 12].Interior.Color = ColorTranslator.FromHtml(row.BULGARO.BackColor);
                //ws.Cells[i + 1, 13].Interior.Color = ColorTranslator.FromHtml(row.TURCO.BackColor);
                //ws.Cells[i + 1, 14].Interior.Color = ColorTranslator.FromHtml(row.GRECO.BackColor);
            }
        }

        static void setHeaders(Worksheet ws, string idHead)
        {
            //ws.Range["A0", "N0"].Insert();

            //var row = ws.Cells.Rows[0];
            (ws.Cells[1, 1] as Range).Value = idHead;
            (ws.Cells[1, 2] as Range).Value = "ITALIANO";
            (ws.Cells[1, 3] as Range).Value = "INGLESE";
            (ws.Cells[1, 4] as Range).Value = "FRANCESE";
            (ws.Cells[1, 5] as Range).Value = "TEDESCO";
            (ws.Cells[1, 6] as Range).Value = "SPAGNOLO";
            (ws.Cells[1, 7] as Range).Value = "RUSSO";
            (ws.Cells[1, 8] as Range).Value = "CINESE";
            (ws.Cells[1, 9] as Range).Value = "RUMENO";
            (ws.Cells[1, 10] as Range).Value = "PORTOGHESE";
            (ws.Cells[1, 11] as Range).Value = "POLACCO";
            (ws.Cells[1, 12] as Range).Value = "BULGARO";
            (ws.Cells[1, 13] as Range).Value = "TURCO";
            (ws.Cells[1, 14] as Range).Value = "GRECO";

            (ws.Cells[1, 1] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 2] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 3] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 4] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 5] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 6] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 7] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 8] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 9] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 10] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 11] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 12] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 13] as Range).Interior.Color = Color.Gray;
            (ws.Cells[1, 14] as Range).Interior.Color = Color.Gray;
        }

        public static void Opc(string xml, string filePath)
        {
            throw new NotImplementedException();
        }
    }

    public static class LoadHelper
    {
        static Application app = new Application();

        public static string Language(string file)
        {
            try
            {

                //var wb = app.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
                //var wb = app.Workbooks.Open(file, Type.Missing, false, Type.Missing, Type.Missing, Type.Missing, true, 
                //    Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, Type.Missing, true, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                //var wb = app.Workbooks.Open(file, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlCorruptLoad.xlExtractData); // if working with another excel

                //var wb = app.Workbooks.Open(file, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, Microsoft.Office.Interop.Excel.XlCorruptLoad.xlExtractData);
                var wb = app.Workbooks.Open(file, 0, true, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", false, false, 0, true, false, Microsoft.Office.Interop.Excel.XlCorruptLoad.xlExtractData);// xlExtractData);
                var translations = wb.Worksheets[1] as Worksheet;
                var maintenance = wb.Worksheets[2] as Worksheet;
                var groups = wb.Worksheets[3] as Worksheet;
                var frequency = wb.Worksheets[4] as Worksheet;

                var xls = new LanguageXls();

                var createRow = new Func<Range, LangRows>((row) =>
                {
                    var createCell = new Func<int, LangCell>((i) =>
                    {
                        var cell = (row[1] as Range).Cells[i];
                        var cellValue = cell.Value?.ToString();
                        var color = (int)cell.Interior.Color;
                        return new LangCell() { CellValue = cellValue, BackColor = ColorTranslator.ToHtml(Color.FromArgb(color)) };
                    });

                    return new LangRows()
                    {
                        ID = (row[1] as Range).Cells[1].Value.ToString(),
                        ITALIANO = createCell(2),
                        INGLESE = createCell(3),
                        FRANCESE = createCell(4),
                        TEDESCO = createCell(5),
                        SPAGNOLO = createCell(6),
                        RUSSO = createCell(7),
                        CINESE = createCell(8),
                        RUMENO = createCell(9),
                        PORTOGHESE = createCell(10),
                        POLACCO = createCell(11),
                        BULGARO = createCell(12),
                        TURCO = createCell(13),
                        GRECO = createCell(14),
                    };
                });

                for (int i = 2; i < translations.UsedRange.Rows.Count; i++)
                {
                    var row = translations.Rows[i];
                    xls.Translations.Add(createRow(row));
                }

                for (int i = 2; i < maintenance.UsedRange.Rows.Count; i++)
                {
                    var row = maintenance.Rows[i];
                    xls.Maintenance.Add(createRow(row));
                }

                for (int i = 2; i < groups.UsedRange.Rows.Count; i++)
                {
                    var row = groups.Rows[i];
                    xls.Group.Add(createRow(row));
                }

                for (int i = 2; i < frequency.UsedRange.Rows.Count; i++)
                {
                    var row = frequency.Rows[i];
                    xls.Frequency.Add(createRow(row));
                }


                var sb = new StringBuilder();
                var settings = new XmlWriterSettings()
                {
                    OmitXmlDeclaration = true,
                    Indent = true,

                };

                using (var wr = XmlTextWriter.Create(sb, settings))
                {
                    var ser = new XmlSerializer(typeof(LanguageXls));
                    ser.Serialize(wr, xls);
                }

                return sb.ToString();
            }
            catch (Exception exc)
            {
                
                return "";
            }
            finally
            {
                app.Workbooks.Close();
            }
        }

        public static string Opc(string file)
        {
            throw new NotImplementedException();
        }
    }
}
