﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Excel2Xml
{
    public class LangCell
    {
        [XmlAttribute]
        public string BackColor { get; set; }
        [XmlText]
        public string CellValue { get; set; }

        public LangCell() { BackColor = "0x000000"; }
    }

    [XmlType]
    public class LangRows
    {
        [XmlAttribute]
        public string ID { get; set; }

        public LangCell ITALIANO { get; set; }
        public LangCell INGLESE { get; set; }
        public LangCell FRANCESE { get; set; }
        public LangCell TEDESCO { get; set; }
        public LangCell SPAGNOLO { get; set; }
        public LangCell RUSSO { get; set; }
        public LangCell CINESE { get; set; }
        public LangCell RUMENO { get; set; }
        public LangCell PORTOGHESE { get; set; }
        public LangCell POLACCO { get; set; }
        public LangCell BULGARO { get; set; }
        public LangCell TURCO { get; set; }
        public LangCell GRECO { get; set; }
    }

    [XmlType]
    public class LanguageXls
    {
        public LanguageXls()
        {
            Translations = new List<LangRows>();
            Maintenance = new List<LangRows>();
            Group = new List<LangRows>();
            Frequency = new List<LangRows>();
        }

        public List<LangRows> Translations { get; set; }
        public List<LangRows> Maintenance { get; set; }
        public List<LangRows> Group { get; set; }
        public List<LangRows> Frequency { get; set; }
    }
}
