﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Excel2Xml
{
    public class TagRow
    {
        [XmlAttribute]
        public string Tag { get; set; }
        [XmlElement]
        public string Address { get; set; }
        [XmlArray]
        public List<string> Note { get; set; }
    }

    public class OpcTagsXls
    {
        public OpcTagsXls()
        {
            plc4 = new List<TagRow>();
            plc2 = new List<TagRow>();
            plc2_Reworked = new List<TagRow>();
            WamFoam = new List<TagRow>();
            plc4cist = new List<TagRow>();
            plc5 = new List<TagRow>();
        }

        [XmlArray]
        public List<TagRow> plc4 { get; set; }
        [XmlArray]
        public List<TagRow> plc2 { get; set; }
        [XmlArray]
        public List<TagRow> plc2_Reworked { get; set; }
        [XmlArray]
        public List<TagRow> WamFoam { get; set; }
        [XmlArray]
        public List<TagRow> plc4cist { get; set; }
        [XmlArray]
        public List<TagRow> plc5 { get; set; }
    }
}
